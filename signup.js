const Admin = require('./models/admin')
const crypt = require('./lib/crypt')
const genRandomString = require('./lib/util').genRandomString
const config = require('./config')

Admin.findOne({ email: process.argv[2] }, (err, user) => {
    if(err)
        throw err

    if(user)
        throw new Error('Username already exists')

    const credParams = {
        iterations: config.iterations,
        keyLength: config.keyLength,
        digest: config.digest,
        saltLength: config.saltLength,
        byteToStringEncoding: config.byteToStringEncoding
    }

    crypt.hash(process.argv[2], process.argv[3], credParams, null, (err, hashedPsw, salt) => {
        if(err)
            throw err

        const newUser = new Admin({
            email: process.argv[2],
            password: hashedPsw.toString(credParams.byteToStringEncoding),
            salt: salt
        })

        newUser.save((err) => {
            if(err)
                throw err
        })
    })
})
