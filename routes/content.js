const express = require('express')

const manage = require('./manage')

const router = express.Router()

router.post('/upload', manage.save)

router.post('/update', manage.update)

router.post('/search', manage.search)

router.post('/retrieve', manage.retrieve)

module.exports = router
