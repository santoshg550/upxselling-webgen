const crypto = require('crypto')

const Admin = require('../models/admin')
const crypt = require('../lib/crypt')
const config = require('../config')

module.exports = function(req, res, next) {
    const reqBody = req.body

    Admin.findOne({ email: reqBody.email }, (err, user) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(!user)
            return res.error(403, 'Username does not exist.')

        const credParams = {
            iterations: config.iterations,
            keyLength: config.keyLength,
            digest: config.digest,
            saltLength: config.saltLength,
            byteToStringEncoding: config.byteToStringEncoding
        }

        crypt.hash(reqBody.email, reqBody.password, credParams, user.salt, (err, key) => {
            if(err)
                return res.error(500)

            if(!crypto.timingSafeEqual(Buffer.from(user.password, credParams.byteToStringEncoding), key)) {
                req.flag = {
                    msg:  'Incorrect password',
                    userId: reqBody.email,
                    password: reqBody.password
                }

                return res.error(401, req.flag.msg)
            }

            req.session.uname = user.email
            req.session._id = user._id

            res.end()
        })
    })
}
