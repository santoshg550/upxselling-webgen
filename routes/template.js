const express = require('express')

const ui = require('./user_interface')

const router = express.Router()

router.get('/info', ui.info)

router.get('/preview/:email', ui.preview)

module.exports = router
