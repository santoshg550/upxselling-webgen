const ejs = require('ejs')

const Template = require('../models/template')
const Content = require('../models/content')

function info(req, res) {
    res.json(req.app.templates)
}

function preview(req, res) {
    Content.findOne({
        email: decodeURIComponent(req.params.email)
    }, (err, contents) => {
        if(err)
            return res.error(500, errors.dbError(err).message);

        if(!contents)
            return res.error(204, 'No such request')

        res.render(contents.prefTemplate, contents)
    })
}

module.exports = {
    info: info,
    preview: preview
}
