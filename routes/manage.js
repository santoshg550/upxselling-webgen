const { Readable } = require('stream')
const mongoose = require('mongoose')

const Content = require('../models/content')

function save(req, res, next) {
    const reqBody = req.body

    Content.findOne({
        email: {
            $regex: reqBody.email,
            $options: 'i'
        },
        prefTemplate: {
            $regex: reqBody.prefTemplate,
            $options: 'i'
        }
    }, (err, content) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(content)
            return res.status(409).end('Request already exists')

        const images = []

        images.push(reqBody.logo, reqBody.banner)

        const listingMenus = reqBody.menus.listing
        listingMenus.forEach((menu) => {
            const items = menu.items

            items.forEach((item) => {
                if(item.image)
                    images.push(item.image)
            })
        })

        let wait = 0

        images.forEach((image) => {
            const readStream = new Readable
            readStream.push(image.data)
            readStream.push(null)

            const bucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db)

            const writeStream = bucket.openUploadStream(image.name)

            readStream
                .pipe(writeStream)
                .on('error', (err) => {
                    console.log(err)
                })
                .on('finish', () => {
                    image.id = writeStream.id

                    if(++wait == images.length)
                        saveReq()
                })
        })

        function saveReq() {
            content = new Content(reqBody)

            content.save((err) => {
                if(err)
                    return res.error(500, errors.dbError(err).message)

                res.end('Content added')
            })
        }
    })
}

function update(req, res, next) {
    const reqBody = req.body

    reqBody.logo = reqBody.logo ? Buffer(reqBody.logo, 'base64') : undefined
    reqBody.banner = reqBody.banner ? Buffer(reqBody.banner, 'base64') : undefined

    Content.findOneAndUpdate({
        email: reqBody.email,
        prefTemplate: reqBody.prefTemplate
    }, reqBody, (err) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        res.end('Content updated')
    })
}

function search(req, res, next) {
    Content.find({
        email: {
            $regex: req.body.email,
            $options: 'i'
        }
    }, (err, contents) => {
        if(err)
            return res.error(500, errors.dbError(err).message);

        if(!contents.length)
            return res.error(204, 'No such request')

        // res.render('partials/search_res.ejs', {
        //     pageReqs: pageReqs
        // })

        res.json(contents)
    })
}

function retrieve(req, res, next) {
    Content.find({
        email: {
            $regex: req.body.email,
            $options: 'i'
        }
    }, (err, contents) => {
        if(err)
            return res.error(500, errors.dbError(err).message);

        if(contents.length == 0)
            return res.status(204).end('No such request')

        res.json(contents)
    })
}

module.exports = {
    save: save,
    update: update,
    search: search,
    retrieve: retrieve
}
