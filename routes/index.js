const express = require('express')

const signin = require('./signin')

const router = express.Router()

router.post('/signin', signin)

router.use((req, res, next) => {
    if(req.session.uname)
        return next()

    res.render('signin', { title : 'Upxselling Web Gen' })
})

router.get('/', (req, res, next) => {
    res.render('home', { title : 'Upxselling Web Gen' })
})

router.get('/signout', (req, res, next) => {
    req.session.destroy((err) => {
        if(err) {
            req.flag = {
                msg: 'Session can not be destroyed.'
            }

            return res.error(500, req.flag.msg)
        }

        res.render('signin', { title : 'Upxselling Web Gen' })
    })
})

module.exports = router
