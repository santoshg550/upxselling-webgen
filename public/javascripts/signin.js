let app = angular.module('webgen', [])

app.controller('signin', ($scope, $http, $window) => {
    $scope.signin = () => {
        $http.post('/signin', {
            email: $scope.email,
            password: $scope.password,
        }).then((res) => {
            $window.location.href = '/'
        }, (res) => {
            alert(res.data.message)
        })
    }
})
