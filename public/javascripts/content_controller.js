$(document).ready(function() {
    const dateFormat = {
        dateFormat: 'yy-mm-dd'
    }

    $('#datepicker-1').datepicker(dateFormat)

    /*
     * For some reason $window is not working
     */
    $('#templates').find(':first-child').remove()
})

const app = angular.module('webgen', [])

app.factory('loadTemplates', function($http) {
    return {
        getTemps: () => {
            return $http.get('template/info/')
        }
    }
})

app.directive('fileModel', ($parse) => {
    return {
        restrict: 'A',
        link: (scope, element, attrs) => {
            const model = $parse(attrs.fileModel)
            const modelSetter = model.assign

            element.bind('change', () => {
                scope.$apply(() => {
                    modelSetter(scope, element[0].files[0])
                })
            })
        }
    }
})

app.controller('webgenCtrl', function($scope, $http, $compile, $window, $location, loadTemplates) {
    $scope.showReqPanel = false
    $scope.viewReq = false

    loadTemplates.getTemps().then((res) => {
        $scope.tempAvail = res.data
    }, (res) => {
        console.log(res)
    })

    $scope.reqPanel = function(panel) {
        $scope.closePanel()

        switch(panel) {
            case 'newReq':
                $scope.showReqPanel = true
                $scope.panelTitle = 'New request'
                $scope.submitAction = 'Add'

                for (el of $('.req-panel .form-control'))
                    $(el).val('')

                break

            case 'editReq':
                $scope.showReqPanel = true
                $scope.panelTitle = 'Edit request'
                $scope.submitAction = 'Update'
                break

            case 'viewReq':
                $scope.viewReq = true

                break
        }

        $scope.panelAction = panel
    }

    $scope.closePanel = function() {
        const curPanel = $('.col-sm-8 > div').not('.ng-hide').attr('ng-show')

        if(curPanel != undefined)
            $scope[curPanel] = false
    }

    $scope.search = function() {
        $http.post('content/search/', {
            email: $scope.clientEmail,
        }).then(function success(res) {
            alert(JSON.stringify(res.data, null, 4))
            // const newElm = angular.element(res.data)
            // $compile(newElm)($scope)

            // $('.search-res .panel-body').empty()
            // $('.search-res .panel-body').prepend(newElm)
        }, function failure(res) {
            alert('Failed to submit request')
        })
    }

    $scope.submitContent = function() {
        const images = []

        const logo = { file: $scope.logo }

        const banner = { file: $scope.banner }

        images.push(logo, banner)

        /*
         * For menus
         */
        const menus = { nonListing: [], listing: [] }

        const nonListingMenus = $('.non-listing-menu > fieldset')
        $.each(nonListingMenus, (index, menu) => {
            menus.nonListing.push({
                name: $(menu).find('.form-group input:first-child').val(),
                contents: (() => {
                    const values = []

                    const contents = $(menu).find('.contents input')

                    $.each(contents, (index, con) => {
                        if($(contents).val())
                            values.push($(contents).val())
                    })

                    return values.length ? values : undefined
                })()
            })
        })

        const listingMenus = $('.listing-menu > fieldset')
        $.each(listingMenus, (index, menu) => {
            menus.listing.push({
                name: $(menu).find('.form-group input:first-child').val(),
                items: (() => {
                    const values = []

                    const items = $(menu).find('.items')

                    $.each(items, (index, item) => {
                        const image = {
                            file: $(item).find('.imgInput')[0].files[0]
                        }

                        if(image.file)
                            images.push(image)

                        values.push({
                            name: $(item).find('input:nth-child(1)').val(),
                            image: image.file ? image : undefined,
                            contents: (() => {
                                const values = []

                                const contents = $(item).find('.contents input')

                                $.each(contents, (index, con) => {
                                    if($(con).val())
                                        values.push($(con).val())
                                })

                                return values.length ? values : undefined
                            })()
                        })
                    })

                    return values
                })()
            })
        })

        let wait = 0

        images.forEach((image) => {
            if(!image.file && ++wait)
                return 

            const fileReader = new FileReader()

            fileReader.onload = () => {
                image.name = image.file.name
                image.data = fileReader.result

                delete image.file

                if(++wait == images.length)
                    sendReq()
            }

            fileReader.readAsDataURL(image.file)
        })

        const established = $scope.established.split('-').map(function(x) {
            return Number(x)
        })

        const today = new Date

        if(established[0] > today.getFullYear() || established[1] > (today.getMonth() + 1) || established[2] > today.getDate())
            return alert('Date of establishment is into the future')

        $scope.pincode = Number($scope.pincode)
        if(isNaN($scope.pincode))
            return alert('Pincode must be numberic')

        const endpoint = $scope.panelAction == 'newReq' ? 'upload' : 'update'

        $scope.selectTemp = function() {
            $scope.prefTemplate = $(this).children('option:selected').val()
        }

        function sendReq() {
            $http.post('content/' + endpoint + '/', {
                name: $scope.name,
                email: $scope.email,
                prefTemplate: $scope.prefTemplate,
                aboutUs: $scope.aboutUs,
                tagline: $scope.tagline,
                address: {
                    line: $scope.line,
                    country: $scope.country,
                    state: $scope.state,
                    city: $scope.city,
                    pincode: $scope.pincode
                },
                established: $scope.established,
                description: $scope.description,
                logo: logo,
                banner: banner,
                menus: menus
            }).then(function success(res) {
                alert('Request submitted')
            }, function failure(res) {
                alert(res.data)
            })
        }
    }

    $scope.editReq = function(email) {
        $http.post('content/retrieve/', {
            email: email
        }).then((res) => {
            $scope.name = res.data[0].name
            $scope.email = res.data[0].email
            $scope.prefTemplate = res.data[0].prefTemplate,
            $scope.aboutUs = res.data[0].aboutUs
            $scope.tagline = res.data[0].tagline

            $scope.line = res.data[0].address.line
            $scope.country = res.data[0].address.country
            $scope.state = res.data[0].address.state
            $scope.city = res.data[0].address.city
            $scope.pincode = res.data[0].address.pincode

            $scope.established = res.data[0].established
            $scope.description = res.data[0].description

            $scope.reqPanel('editReq')
        }, (res) => {
            alert(res.data)
        })
    }

    $('.imgInput').change((event) => {
        $(event.target.parentNode).siblings('span').html(event.target.files[0].name)
    })

    /*
     * Passing 'this' from html does not work
     */
    $scope.clearMenuImg = function($event) {
        $($event.target).siblings('label').find('> input')[0].value = ''
        $($event.target).siblings('span').empty()
    }

    $scope.preview = function(email) {
        $window.open($window.location.href + 'template/preview/' + encodeURIComponent(email))
    }

    $scope.newContentInput = function($event) {
        $($event.target.parentNode.parentNode).find('input:last-child').clone().appendTo($event.target.parentNode.parentNode)
    }

    $scope.remContentInput = function($event) {
        const inputs = $($event.target.parentNode.parentNode).find('input')

        if(inputs.length == 1)
            return

        inputs[inputs.length - 1].remove()
    }
})
