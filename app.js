const express = require('express')
const bodyParser = require('body-parser')
const session = require('express-session')
const compression = require('compression');
const MongoStore = require('connect-mongo')(session)
const helmet = require('helmet');
const mongoose = require('mongoose')
const responseTime = require('response-time');
const addRequestId = require('express-request-id');

const log = require('./lib/log')
const mail = require('./lib/mail')
const config = require('./config')
const rateLimiter = require('./rate_limiter')

const index = require('./routes/index')
const content = require('./routes/content')
const template = require('./routes/template')

const app = express()

app
    .set('view engine', 'ejs')
    .use(compression())
    .use(express.static('public'))
    .use(bodyParser.json({
        limit: '100mb'
    }))
    .use(helmet())
    .use(helmet.contentSecurityPolicy({
        directives: {
            defaultSrc: ["'self'"],
            scriptSrc: [
                "'self'",
                "'unsafe-eval'",
                'ajax.googleapis.com',
                'maxcdn.bootstrapcdn.com',
                'code.jquery.com'
            ],
            styleSrc: [
                "'self'",
                "'unsafe-inline'",
                'ajax.googleapis.com',
                'maxcdn.bootstrapcdn.com',
                'cdnjs.cloudflare.com',
                'code.jquery.com'
            ],
            imgSrc: [
                "'self'",
                'code.jquery.com'
            ],
            fontSrc: [
                'cdnjs.cloudflare.com',
                'maxcdn.bootstrapcdn.com'
            ],
            connectSrc: ["'self'"]
        },
        setAllHeaders: true
    }))
    .use(helmet.noCache())
    .use(session({
        name: 'sessionId',
        secret: config.sessionSecret,
        store: new MongoStore({
            mongooseConnection: mongoose.connection,
            stringify: true
        }),
        saveUninitialized: true,
        resave: false
    }))
    .use(errors.error)
    .disable('x-powered-by')

app
    .use(addRequestId())
    .use(responseTime())
    .use((req, res, next) => {
        log.info({ req: req })

        res.header('X-Request-Id', req.id)

        function afterRes() {
            res.removeListener('finish', afterRes)
            res.removeListener('close', afterRes)

            log.info({ res: res })
        }

        res.on('finish', afterRes)
        res.on('close', afterRes)

        next()
    })
    .use('/signin', rateLimiter())
    .use('/', index)
    .use('/content', content)
    .use('/template', template)
    .use((req, res, next) => {
        const err = new Error('Not Found')
        err.status = 404
        next(err)
    })
    .use((err, req, res, next) => {
        log.error({ err: err }, 'Error handling middleware')

        res.status(err.status || 500).render('error', {
            message: err.message
        })
    })

module.exports = app
