const mongoose = require('mongoose')
const EventEmitter = require('events')

const config = require('./config')

function Connector() {
    this.connect = () => {
        mongoose.connect(config.databaseURI, {
            reconnectTries: Number.MAX_VALUE,
            reconnectInterval: 1000,

            useNewUrlParser: true
        })

        this.db = mongoose.connection
            .on('connected', () => {
                this.emit('connected')
            })
            .on('error', (err) => {
                this.emit('error', err)
            })
            .on('disconnected', () => {
                this.emit('disconnected')
            })
    }

    this.disconnect = (callback) => {
        this.db.close(callback)
    }
}

Connector.prototype = Object.create(EventEmitter.prototype)

module.exports = Connector
