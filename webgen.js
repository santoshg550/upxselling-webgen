#!/usr/bin/env node

const http = require('http')
const fs = require('fs')
const blocked = require('blocked')

const Connector = require('./connections')
const updateApp = require('./update_app')
const config = require('./config')
const log = require('./lib/log')

errors = require('./lib/errors')
buildError = errors.buildError

const app = require('./app')

blocked((ms) => {
    log.info('BLOCKED FOR ' + (ms | 0) + 'ms')
})

function begin(callback) {
    log.info('Waiting for connections...')

    let server

    function onConn(err) {
        if(err)
            return shutdown(err) 

        log.info('Services connected')

        app.on('updated', createServer)
        updateApp(app)

        function createServer(err) {
            if(err)
                return shutdown(err)

            log.info('App updated')

            server = http.createServer(app)

            server.on('error', shutdown)
            server.listen(config.port, onListen)
        }

        function onListen() {
            log.info('Server is listening on port ' + this.address().port)

            if(callback)
                callback(this)

            // For pm2
            if(process.send)
                process.send('ready')
        }
    }

    function shutdown(err, p) {
        if(err)
            log.error({ err: err, promise: p ? true : undefined }, 'Server error')

        connector.disconnect((err) => {
            if(err)
                log.error({ err: err }, 'Error disconnecting services')

            if(server)
                server.close(() => {
                    log.info('Server is shutting down')

                    setTimeout(() => {
                        process.exit(err ? 1 : 0)
                    }, 2000)
                })
        })
    }

    function onClose() {
        log.info('Connection closed to services')
    }

    function onDisconnect() {
        log.fatal('Disconnected from services')
    }

    process.on('SIGTERM', () => {
        log.info('SIGTERM received')
        shutdown()
    })

    process.on('SIGINT', () => {
        log.info('SIGINT received')
        shutdown()
    })

    process.on('uncaughtException', shutdown)
    process.on('unhandledRejection', shutdown)

    const connector = new Connector

    connector
        .on('error', shutdown)
        .on('connected', onConn)
        .on('closed', onClose)
        .on('disconnected', onDisconnect)

    connector.connect()
}

if(require.main == module)
    begin()
else
    module.exports = begin
