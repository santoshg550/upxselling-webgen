const mongoose = require('mongoose')

const Schema = mongoose.Schema

const content = new Schema({
    prefTemplate: String,
    name: String,
    email: String,
    description: String,
    aboutUs: String,
    tagline: String,
    images: [Buffer],
    address: {
        line: String,
        country: String,
        state: String,
        city: String,
        pincode: Number
    },
    established: Date,
    logo: {
        name: String,
        id: String
    },
    banner: {
        name: String,
        id: String
    },
    menus: {
        nonListing: [
            {
                title: String,
                contents: [String]
            }
        ],
        listing: [
            {
                title: String,
                items: [
                    {
                        image: {
                            name: String,
                            id: String
                        },
                        title: String,
                        contents: [String]
                    }
                ]
            }
        ]
    },
    whatsapp: [String],
    contacts: [String],
    updatedOn: Date
})

content.pre('save', function(next) {
    this.updatedOn = new Date

    next()
})

module.exports = mongoose.model('Content', content)
