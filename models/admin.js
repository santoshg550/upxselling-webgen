const mongoose = require('mongoose')
const Schema = mongoose.Schema

const adminSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    role: String,
    lastLogin: Date,
    updatedOn: Date
})

adminSchema.pre('save', function(next) {
    this.updatedOn = new Date

    next()
})

module.exports = mongoose.model('Admin', adminSchema)
