const mongoose = require('mongoose')

const Schema = mongoose.Schema

const template = new Schema({
    name: String,
    id: {
        type: String,
        unique: true
    },
    price: Number,
    updatedOn: Date
})

template.pre('save', function(next) {
    this.updatedOn = new Date

    next()
})

module.exports = mongoose.model('Template', template)
