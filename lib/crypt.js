var crypto = require('crypto');

var log = require('./log');
var genRandomString = require('./util').genRandomString;

function hash(id, data, options, salt, callback) {
    if(salt)
        return onSalt(null, salt)

    genRandomString(options.saltLength, options.byteToStringEncoding, onSalt)

    function onSalt(err, salt) {
        if(err)
            return callback(err)

        crypto.pbkdf2(data, salt, options.iterations, options.keyLength, options.digest, (err, key) => {
            if(err)
                log.fatal({
                    id: id,
                    data: data,
                    error: err.stack
                }, 'Failed to hash')

            callback(err, key, salt)
        })
    }
}

module.exports = {
    hash: hash
}
