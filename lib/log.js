var bunyan = require('bunyan')

const logger = {
    name: 'webgen',
    serializers: {
        err: errSerializer,
        req: reqSerializer,
        res: resSerializer
    }
}

if(['local', 'IaaS'].indexOf(process.env.PLATFORM) >= 0)
    logger.streams = [{
        type: 'rotating-file',
        path: process.cwd() + '/webgen.log',
        period: '1d',
        count: 1
    }]

const log = bunyan.createLogger(logger)

log.on('error', (err, stream) => {
    console.log(err)
})

function reqSerializer(req) {
    return {
        id: req.id,
		method: req.method,
        headers: req.headers,
		url: req.url,
		remoteAddress: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
		remotePort: req.connection.remotePort
    }
}

function resSerializer(res) {
    return {
        id: res.get('x-request-id'),
        latency: res.get('x-response-time'),
        status: res.statusCode,
        message: res.statusMessage
    }
}

function errSerializer(err) {
    return {
        name: err.name,
        message: err.message,
        stack: err.stack
    }
}

module.exports = log
