const Template = require('./models/template')

module.exports = (app) => {
    let update = 1

    Template.find((err, templates) => {
        if(err)
            return app.emit('updated', err)

        if(!templates)
            return app.emit('updated', new Error('Template info is absent in database'))

        app.templates = templates

        if(--update == 0)
            app.emit('updated')
    })
}
