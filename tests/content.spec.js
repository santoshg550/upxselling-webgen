const fs = require('fs')

const chai = require('chai')
const chaiHttp = require('chai-http')

const Content = require('../models/content')
const { prep, cleanDB } = require('./prep')
const validations = require('../routes/validations')
const signin = require('./before_all').signin

const should = chai.should()
const expect = chai.expect

chai.use(chaiHttp)

describe('client content', function() {
    let server
    let cookie

    before((done) => {
        prep((svr) => {
            server = svr
            signin('santoshg550@gmail.com', 'Santosh@123', server, (ck) => {
                cookie = ck
                done()
            })
        })
    })

    describe('upload', function() {
        const content = {
            prefTemplate: 'free_world',
            name: 'xyz',
            email: 'santoshg550@gmail.com',
            description: 'Something something',
            aboutUs: 'We are something',
            tagline: 'Make the world a better place',
            established: new Date(2014, 09, 04),

            address: {
                line: 'Road no 1',
                country: 'India',
                state: 'Maharashtra',
                city: 'Pune',
                pincode: 3030030
            },

            logo: {
                name: 'valid_logo.png',
                data: fs.readFileSync('tests/valid_logo.png').toString('base64')
            },

            banner: {
                name: 'banner.jpeg',
                data: fs.readFileSync('tests/banner.jpeg').toString('base64')
            },

            menus: {
                nonListing: [
                    {
                        title: 'Packages',
                        contents: [
                            'For family',
                            'For couples'
                        ]
                    }
                ],
                listing: [
                    {
                        title: 'Destinations',
                        items: [
                            {
                                image: {
                                    name: 'content.jpg',
                                    data: fs.readFileSync('tests/content.jpg').toString('base64')
                                },
                                title: 'Maharashtra',
                                contents: [
                                    'Lonawala',
                                    'Khandala'
                                ]
                            },
                            {
                                image: {
                                    name: 'content.jpg',
                                        data: fs.readFileSync('tests/content.jpg').toString('base64')
                                },
                                    title: 'Maharashtra',
                                    contents: [
                                        'Lonawala',
                                        'Khandala'
                                    ]
                            }
                        ]
                    }
                ]
            },

            whatsapp: [
                '1234567890',
                '0123456789'
            ],
            contacts: [
                '1234567890',
                '0123456789'
            ]
        }

        it('it should upload the content', ((done) => {
            chai
                .request(server)
                .post('/content/upload')
                .set('Cookie', cookie)
                .send(content)
                .end(function(err, res) {
                    expect(err).to.be.null
                    expect(res).to.have.status(200)

                    done()
                })
        }))

        it('it should update the content', ((done) => {
            content.tagline = 'Rise above'

            chai
                .request(server)
                .post('/content/update')
                .set('Cookie', cookie)
                .send(content)
                .end(function(err, res) {
                    expect(err).to.be.null
                    expect(res).to.have.status(200)

                    done()
                })
        }))
    })

    describe('search for page req', function() {
        it('it should fetch the page req', ((done) => {
            chai
                .request(server)
                .post('/content/search')
                .set('Cookie', cookie)
                .send({
                    email: 'santoshg550@gmail.com'
                })
                .end(function(err, res) {
                    expect(err).to.be.null

                    expect(res).to.have.status(200)
                    expect(res).to.have.header('content-type', /text\/html/)

                    done()
                })
        }))
    })

    describe('retrieve page contents', function() {
        it('it should not fetch page contents', ((done) => {
            chai
                .request(server)
                .post('/content/retrieve')
                .set('Cookie', cookie)
                .send({
                    email: 'yyyyyyyyyy@gmail.com'
                })
                .end(function(err, res) {
                    expect(err).to.be.null

                    expect(res).to.have.status(204)

                    done()
                })
        }))

        it('it should fetch page contents', ((done) => {
            chai
                .request(server)
                .post('/content/retrieve')
                .set('Cookie', cookie)
                .send({
                    email: 'santoshg550@gmail.com'
                })
                .end(function(err, res) {
                    expect(err).to.be.null

                    expect(res).to.have.status(200)
                    expect(res.body).to.be.an('array')

                    done()
                })
        }))
    })
})
