let mongoose

function prep(callback) {
    if(process.env.NODE_ENV == 'dev')
        require('dotenv/config')

    /*
     * Using server.on('listening', callback) causes callback to be invoked
     * multiple times
     */
    require('../webgen')(function(server) {
        mongoose = require('mongoose')

        callback(server)
    })
}

function cleanDB() {
    for(let coll in mongoose.connection.collections)
        mongoose.connection.collections[coll].deleteOne(function() { })
}

module.exports = {
    prep: prep,
    cleanDB: cleanDB
}
